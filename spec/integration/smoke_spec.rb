# encoding: utf-8

require 'spec_helper'

describe SoundCouch do
  let(:couch) do
    SoundCouch.new('https://soundcloud.com/lars-dev',
                   ENV['SOUNDCLOUD_ACCESS_TOKEN'])
  end

  before do
    couch.all_docs.each { |doc| couch.delete(doc) }
  end

  it_behaves_like 'document API'
end

describe Couch do
  let(:couch) do
    Couch.new('http://localhost:5984/soundcloud')
  end

  before do
    couch.destroy
    couch.create
  end

  it_behaves_like 'document API'
end
