# encoding: utf-8

require 'spec_helper'

describe Couch::Replication do
  let(:couchdb) do
    Couch.new('http://localhost:5984/soundcloud')
  end

  let(:soundcloud) do
    SoundCouch.new('https://soundcloud.com/lars-dev',
                   ENV['SOUNDCLOUD_ACCESS_TOKEN'])
  end

  before do
    soundcloud.all_docs.each { |doc| soundcloud.delete(doc) }

    couchdb.destroy
    couchdb.create
  end

  let(:audio_data) { File.read('spec/fixtures/1sec.mp3') }
  let(:document_template) do
    {
      'title' => 'My document',
      '_attachments' => {
        '1sec.mp3' => {
          'content_type' => 'audio/mpeg',
          'digest' => 'md5-' + Digest::MD5.base64digest(audio_data),
          'data' => Base64.encode64(audio_data)
        }
      }
    }
  end

  describe 'from CouchDB to SoundCloud' do
    let!(:couchdb_doc) { couchdb.post(document_template) }

    subject! { Couch.replicate(couchdb, soundcloud) }

    let!(:soundcloud_doc) { soundcloud.get(couchdb_doc) }

    it 'bumps _rev because track.uri is added' do
      expect(soundcloud_doc['_id']).to eq(couchdb_doc['_id'])
      expect(soundcloud_doc['_rev']).to start_with('2-')
      expect(soundcloud_doc['uri']).to start_with('https://api.soundcloud.com/tracks/')
    end

    let!(:track) do
      soundcloud.client.request(:get, soundcloud_doc['uri']).json
    end

    it 'stores _id and current _rev in track.tag_list' do
      expect(track['tag_list']).to include('couch:id=' + couchdb_doc['_id'])
      expect(track['tag_list']).to include('couch:rev=' + couchdb_doc['_rev'])
    end
  end

  describe 'from SoundCloud to CouchDB' do
    let(:id)             { 'bfc34cbf43864cf6adf99ca6d49a2b40' }
    let(:rev)            { '123-d41d8cd98f00b204e9800998ecf8427e' }
    let(:tag_list)       { sprintf('couch:id=%s couch:rev=%s', id, rev) }
    let(:track_template) { { 'title' => 'My track', 'tag_list' => tag_list } }

    let(:track) do
      soundcloud.client.upload(track_template, '1sec.mp3', audio_data).json
    end

    before { soundcloud.wait(track, 1.0) }

    subject! { Couch.replicate(soundcloud, couchdb) }

    let(:document) { couchdb.get('_id' => id) }

    it 'reads _id and _rev from track.tag_list' do
      expect(document['_id']).to eq(id)
      expect(document['_rev']).to start_with('124-')
    end

    describe 'with insufficient track.tag_list' do
      let(:tag_list) { '' }
      let(:id)       { Digest::MD5.hexdigest(track['id'].to_s) }

      it 'generates _id based on track.id' do
        expect(document['_id']).to eq(id)
      end
    end
  end
end
