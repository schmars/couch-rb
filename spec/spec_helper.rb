# encoding: utf-8

if ENV['COVERAGE'] == 'true'
  require 'simplecov'
  require 'coveralls'

  SimpleCov.formatter = SimpleCov::Formatter::MultiFormatter[
    SimpleCov::Formatter::HTMLFormatter,
    Coveralls::SimpleCov::Formatter
  ]

  SimpleCov.start do
    command_name 'spec:unit'

    add_filter 'config'
    add_filter 'spec'
  end
end

require 'dotenv'
Dotenv.load

require 'couch'
require 'soundcouch'

RSpec.configure do |config|
  config.around do |example|
    HTTPkit.run do
      # EM.add_timer(0.5) { raise 'Example timed out' }
      example.call
    end
  end
end

require 'awesome_print'
require 'pry'

require 'devtools/spec_helper'
