# encoding: utf-8

require 'digest/md5'
require 'base64'

shared_context 'document API' do
  let(:audio_data) { File.read('spec/fixtures/1sec.mp3') }

  let(:document) do
    {
      'title' => 'My document',
      '_attachments' => {
        '1sec.mp3' => {
          'content_type' => 'audio/mpeg',
          'digest' => 'md5-' + Digest::MD5.base64digest(audio_data),
          'data' => Base64.encode64(audio_data)
        }
      }
    }
  end

  it 'saves and retrieves documents' do
    doc = couch.post(document)
    doc2 = couch.get(doc)
    expect(doc['title']).to eq(doc2['title'])
  end
end
