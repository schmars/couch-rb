# encoding: utf-8

class SoundCouch
  class Client
    def initialize(uri, access_token)
      @uri, @access_token = URI(uri), access_token
      @http = HTTPkit::Client.start(config)
    end

    def config
      { address: @uri.host, port: @uri.port, ssl: @uri.scheme == 'https',
        handlers: [HTTPkit::Client::SSL.new,
                   HTTPkit::Client::KeepAlive.new,
                   HTTPkit::Client::Timeouts.new],
        timeout: 120, connect_timeout: 120 }
    end

    def request(http_method, uri, body = nil)
      request = Couch::Request.new(http_method, uri, headers, body)
      response = @http.perform(request).sync
      Couch::Response.build(response)
    end

    def upload(track, filename, asset_data)
      mimetype = 'application/octet-stream'
      io       = UploadIO.new(StringIO.new(asset_data), mimetype, filename)

      request = FormRequest.new(:post, '/tracks', headers,
                                'track' => track.merge('asset_data' => io))
      response = @http.perform(request).sync
      Couch::Response.build(response)
    end

    def headers
      { 'Authorization' => 'OAuth ' + @access_token }
    end
  end
end
