# encoding: utf-8

class SoundCouch
  class FormRequest < HTTPkit::Request
    BOUNDARY = 'SoundCouchBoundary'

    def initialize(http_method, uri, headers = {}, body = nil)
      body &&= begin
        headers['Content-Type'] = 'multipart/form-data; boundary=' + BOUNDARY

        parts = flatten(body).flat_map do |k, v|
          if v.is_a?(Array)
            v.map { |x| Parts::Part.new(BOUNDARY, k, x) }
          else
            Parts::Part.new(BOUNDARY, k, v)
          end
        end + [Parts::EpiloguePart.new(BOUNDARY)]

        CompositeReadIO.new(parts.map(&:to_io)).read
      end

      headers['Accept'] = 'application/json'
      super(http_method, uri, headers, body || '')
    end

    def flatten(body, prefix = '')
      flattened = []
      body.each do |(k,v)|
        if body.is_a?(Array)
          v = k
          k = ""
        end

        flattened_key = prefix == "" ? "#{k}" : "#{prefix}[#{k}]"
        if v.is_a?(Hash) || v.is_a?(Array)
          flattened += flatten(v, flattened_key)
        else
          flattened << [flattened_key, v]
        end
      end
      flattened
    end
  end
end
