# encoding: utf-8

require 'httpkit'
require 'json'

require 'couch/version'
require 'couch/client'
require 'couch/request'
require 'couch/response'
require 'couch/replication'

class Couch
  def initialize(uri)
    @uri = URI(uri)
    @client = Client.new(@uri)
  end

  def destroy
    @client.request(:delete, @uri.path).successful?
  end

  def create
    @client.request(:put, @uri.path).successful?
  end

  def changes(options = {})
    response = @client.request(:get, changes_uri(options))
    response.json['results']
  end

  def bulk_docs(docs, options = {})
    if docs.any?
      body = options.merge(docs: docs)
      @client.request(:post, bulk_docs_uri, body).successful?
    end
  end

  def revs_diff(revs)
    response = @client.request(:post, revs_diff_uri, revs)
    response.json.inject({}) do |a, (id, revs)|
      a.merge(id => revs['missing'])
    end
  end

  def put(doc, options = {})
    response = @client.request(:put, doc_uri(doc['_id'], options), doc)
    if response.successful?
      doc.merge('_rev' => response.json['rev'])
    else
      false
    end
  end

  def post(doc)
    response = @client.request(:post, @uri.path, doc)
    if response.successful?
      doc.merge('_id'  => response.json['id'],
                '_rev' => response.json['rev'])
    else
      false
    end
  end

  def get(doc, options = {})
    response = @client.request(:get, doc_uri(doc['_id'], options))
    if options[:open_revs]
      response.json.map { |row| row.values.first }
    else
      response.json
    end
  end

  def uuids(count)
    response = @client.request(:get, uuids_uri(count))
    response.json['uuids']
  end

  private

  def query(options)
    options.map { |kv| kv.join('=') }.join('&')
  end

  def changes_uri(options = {})
    @uri.path + '/_changes?' + query(options)
  end

  def bulk_docs_uri
    @uri.path + '/_bulk_docs?'
  end

  def revs_diff_uri
    @uri.path + '/_revs_diff'
  end

  def doc_uri(id, options)
    options[:open_revs] &&= URI.escape(JSON.generate(options[:open_revs]))
    @uri.path + '/' + id + '?' + query(options)
  end

  def uuids_uri(count)
    '/_uuids?count=' + count.to_s
  end
end
