# encoding: utf-8

class Couch
  class Response < HTTPkit::Response
    def self.build(response)
      new(response.status, response.headers, response.body)
    end

    def json
      @json ||= JSON.parse(body.to_s)
    end
  end
end
