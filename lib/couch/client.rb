# encoding: utf-8

class Couch
  class Client
    def initialize(uri)
      @uri = uri
      @http = HTTPkit::Client.start(config)
    end

    def config
      { address: @uri.host, port: @uri.port, ssl: @uri.scheme == 'https',
        handlers: [HTTPkit::Client::SSL.new,
                   HTTPkit::Client::KeepAlive.new,
                   HTTPkit::Client::Timeouts.new],
        timeout: 120, connect_timeout: 120 }
    end

    def request(http_method, uri, body = nil)
      request = Request.new(http_method, uri, {}, body)
      response = @http.perform(request).sync
      Response.build(response)
    end
  end
end
