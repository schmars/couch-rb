# encoding: utf-8

class Couch
  class Request < HTTPkit::Request
    def initialize(http_method, uri, headers = {}, body = nil)
      body &&= begin
        headers['Content-Type'] = 'application/json'
        JSON.generate(body)
      end

      headers['Accept'] = 'application/json'
      super(http_method, uri, headers, body || '')
    end
  end
end
