# encoding: utf-8

class Couch
  def self.replicate(source, target)
    Replication.new(source, target).replicate
  end

  class Replication
    def initialize(source, target)
      @source, @target = source, target
      @revs_diff = @target.revs_diff(changed_revs(@source.changes))
    end

    def changed_revs(changes)
      changes.reduce({}) do |a, e|
        id, revs = e['id'], e['changes'].map { |c| c['rev'] }
        a.merge(id => a[id].to_a + revs)
      end
    end

    def replicate
      docs = @revs_diff.flat_map do |id, revs|
        @source.get({ '_id' => id }, open_revs: revs, attachments: true) || nil
      end
      @target.bulk_docs(docs.compact, new_edits: false)
    end
  end
end
