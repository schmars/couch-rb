# encoding: utf-8

require 'httpkit'
require 'couch'

require 'parts'
require 'composite_io'

require 'soundcouch/client'
require 'soundcouch/form_request'

class SoundCouch
  ReadOnlyError = Class.new(RuntimeError)

  attr_reader :client

  def initialize(permalink_url, access_token)
    @client = Client.new('https://api.soundcloud.com', access_token)

    @user = @client.request(:get, resolve(permalink_url)).json
    @me = @client.request(:get, '/me').json

    @index = {}
  end

  def changes
    all_docs.map do |doc|
      { 'seq'     => 0,
        'id'      => doc['_id'],
        'changes' => ['rev' => doc['_rev']] }
    end
  end

  def all_docs(options = {})
    response = @client.request(:get, @user['uri'] + '/tracks')
    response.json.map { |track| document(track, options) }
  end

  def bulk_docs(docs, _options = {})
    docs.map do |doc|
      doc['uri'].nil? ? post(doc) : put(doc)
    end
  end

  def revs_diff(revs, options = {})
    docs = all_docs(options)
    revs.reject do |id, rev_ids|
      doc = docs.detect { |d| d['_id'] == id }
      doc && rev_ids.include?(doc['_rev'])
    end
  end

  def get(doc, options = {})
    response = @client.request(:get, doc_uri(doc))
    if response.successful?
      document(response.json, options)
    else
      false
    end
  end

  def delete(doc)
    @index.clear
    response = @client.request(:delete, doc_uri(doc))
    response.successful?
  end

  def put(doc)
    raise ReadOnlyError if readonly?

    track = relevant_fields(doc)
      .merge(dump_metadata(doc))

    response = @client.request(:put, doc_uri(doc), track)
    document(response.json)
  end

  # FIXME: filename might be optional
  def post(doc)
    raise ReadOnlyError if readonly?

    track    = relevant_fields(doc).merge(dump_metadata(doc))
    filename = doc['_attachments'].keys.first
    data     = filename && doc['_attachments'][filename]['data']

    response = @client.upload(track, filename, data)
    document(response.json)
  end

  def resolve(permalink_url)
    response = @client.request(:get, '/resolve?url=' + permalink_url)
    if response.redirection?
      response.headers['Location']
    else
      false
    end
  end

  def readonly?
    @me['uri'] != @user['uri']
  end

  def wait(doc, interval)
    return unless doc['uri']
    loop do
      response = @client.request(:get, doc['uri'])
      if response.client_error? || response.json['state'] =~ /finished|failed/
        break
      else
        HTTPkit.sleep(interval)
      end
    end
  end

  private

  RELEVANT_FIELDS = ['uri', 'title', 'permalink_url', 'tag_list']

  def relevant_fields(doc_or_track)
    doc_or_track.select { |k, _| RELEVANT_FIELDS.include?(k) }
  end

  def document(track, options = {})
    relevant_fields(track)
      .merge(parse_metadata(track))
      .merge(attachments(track, options))
      .tap { |doc| @index[doc['_id']] = doc['uri'] }
  end

  def parse_tags(str)
    Hash[str.split(/\s+/).map { |pair| pair.split('=') }]
  end

  def dump_tags(hash)
    hash.map { |pair| pair.join('=') }.join(' ')
  end

  def generate_id(track, id)
    id || Digest::MD5.hexdigest(track['id'].to_s)
  end

  def generate_rev(track, last_rev)
    generation, id = last_rev.to_s.split('-')
    current_id     = generate_rev_id(track)

    if id == current_id
      last_rev
    else
      sprintf('%d-%s', generation.to_i + 1, current_id)
    end
  end

  def generate_rev_id(track)
    values = track.values_at(*RELEVANT_FIELDS).map(&:to_s)
    Digest::MD5.hexdigest(JSON.dump(values))
  end

  def parse_metadata(track)
    tags = parse_tags(track['tag_list'].to_s)
    id, rev = tags.delete('couch:id'), tags.delete('couch:rev')
    { '_id' => generate_id(track, id),
      '_rev' => generate_rev(track, rev),
      'tag_list' => dump_tags(tags) }
  end

  def dump_metadata(doc)
    tags = parse_tags(doc['tag_list'].to_s)
      .merge('couch:id' => doc['_id'], 'couch:rev' => doc['_rev'])
    { 'tag_list' => dump_tags(tags) }
  end

  def attachments(track, options)
    filename = 'original.' + track['original_format']
    attachment = { 'content_type' => 'audio/' + track['original_format'] }

    if options[:attachments]
      audio_data = audio(track)
      attachment['digest'] = 'md5-' + Digest::MD5.base64digest(audio_data)
      attachment['data'] = Base64.encode64(audio_data)
    else
      attachment['stub'] = true
    end

    { '_attachments' => { filename => attachment } }
  end

  def audio(track)
    response = @client.request(:get, track['download_url'])
    location = response.headers['Location']

    response = HTTPkit::Client.request(:get, location)
    if response.successful?
      response.body.to_s
    else
      false
    end
  end

  def doc_uri(doc)
    doc['uri'] || @index[doc['_id']]
  end
end
