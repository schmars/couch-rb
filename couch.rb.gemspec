# encoding: utf-8

lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'couch/version'

Gem::Specification.new do |spec|
  spec.name          = 'couch.rb'
  spec.version       = Couch::VERSION
  spec.authors       = ['Lars Gierth']
  spec.email         = ['larsg@systemli.org']
  spec.description   = %q{CouchDB client and replicator for Ruby}
  spec.summary       = %q{CouchDB client and replicator for Ruby}
  spec.homepage      = 'https://github.com/lgierth/couch.rb'
  spec.license       = 'Public Domain'

  spec.files         = `git ls-files`.split($/)
  spec.test_files    = spec.files.grep(%r{^spec/})
  spec.require_paths = ['lib']
  spec.executables   = spec.files.grep(%r{^bin/}) - ['bin/couchdb']

  spec.add_dependency 'httpkit',    '~> 0.6.0.pre.3'
  spec.add_dependency 'promise.rb', '~> 0.6'
  spec.add_dependency 'multipart-post'

  spec.add_development_dependency 'rspec'
end
