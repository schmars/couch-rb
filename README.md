# couch.rb [![Build Status](https://travis-ci.org/lgierth/couch.rb.png?branch=master)](https://travis-ci.org/lgierth/couch.rb) [![Code Climate](https://codeclimate.com/github/lgierth/couch.rb.png)](https://codeclimate.com/github/lgierth/couch.rb) [![Coverage Status](https://coveralls.io/repos/lgierth/couch.rb/badge.png?branch=master)](https://coveralls.io/r/lgierth/couch.rb?branch=master)

CouchDB client and replicator for Ruby.

## Unlicense

couch.rb is free and unencumbered public domain software. For more
information, see [unlicense.org](http://unlicense.org/) or the accompanying
UNLICENSE file.

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
